import { memo, useCallback, useMemo, useRef, useState } from "react";

const childStyle = {
  padding: "10px",
  margin: "10px",
  border: "1px solid black",
};

type T = { text: string; now: Date };

interface MainPage {
  date?: Date;
}

type abType = { a: string; b: string };

type ListProps = { items: Array<T>; ab: abType; onItemClick: (text: string) => void };

const UseCallbackPage = (props: MainPage) => {
  const [items, setItems] = useState<Array<T>>([]);
  const [text, setText] = useState<string>("");

  const [ab, setAb] = useState<abType>({ a: "", b: "" });

  const inputRef = useRef<HTMLInputElement>(null);

  const aRef = useRef<HTMLInputElement>(null);
  const bRef = useRef<HTMLInputElement>(null);

  const addItem = () => {
    setItems([...items, { text: "", now: new Date() }]);
  };

  const handleText = () => {
    setText(inputRef!.current!.value);
  };

  const handleAb = () => {
    setAb({
      a: aRef!.current!.value,
      b: bRef!.current!.value,
    });
  };

  const date = new Date();

  // При помощи useCallback
  // мы сохраняем (мемоизируем) функцию
  // чтобы она не создавалась при перерисовке UseCallbackPage
  // и не перерисовывала List
  // const onItemClick = useCallback(
  const onItemClick = (t: string) => {
    alert("Функция была создана " + date + " " + text);
  };

  return (
    <div>
      <label>Введитет значение </label>
      <br />
      <input ref={inputRef} type='text' />
      <button onClick={handleText}>Добавить текст</button>
      <br />
      {text}
      <br />
      <br />

      <div style={{ margin: "1rem", border: "3px solid blue" }}>
        <label>Потестируем пропсы </label>

        <input ref={aRef} type='text' placeholder='a' />
        <input ref={bRef} type='text' placeholder='b' />

        <button onClick={handleAb}>Добавить AB</button>

        <APlusBPanel ab={ab} />
      </div>

      <br />
      <button onClick={addItem}>Добавить новый элемент</button>

      <List items={items} ab={ab} onItemClick={onItemClick} />
    </div>
  );
};

interface AbPanelProps {
  ab: abType;
}

const areABEqual = (prev: AbPanelProps, cur: AbPanelProps) => {
  return (prev.ab.a + prev.ab.b) === (cur.ab.a + cur.ab.b);
  // return prev.ab.a === cur.ab.a && prev.ab.b === cur.ab.b;
};

// Панель для демонстрации Работы
// const APlusBPanel = memo((props: AbPanelProps) => {
const APlusBPanel = (props: AbPanelProps) => {
  const { ab } = props;
  const { a, b } = ab;

  return (
    <div style={{ fontSize: "2rem" }}>
      {a} + {b} = {a + b}
      <br />
      <label> Текущее Время {new Date().toLocaleString()}</label>
    </div>
  );
};
// }, areABEqual);

// Компонент в memo
// значи перерисовывает при изменении чего либо из props
const List = (props: ListProps) => {
  const { items, ab } = props;

  return (
    <div>
      {items.map((v, i) => (
        <ChildPanel key={i} t={v} onClick={props.onItemClick} />
      ))}
    </div>
  );
};

interface ChildProps {
  t: T;
  onClick: (text: string) => void;
}

// Компонент в котором есть кнопка с изменяемым значением
//const ChildPanel = memo((props: ChildProps) => {
const ChildPanel = (props: ChildProps) => {
  const { onClick } = props;

  const [count, setCount] = useState<number>(0);

  const getRandomColor = (): string => {
    return ((Math.random() * 0xffffff) << 0).toString(16);
  }

  // При помощи useMemo - в случае перерисовки компоненты
  // сохранит свой цвет
  // const randomColour1 = useMemo(() => "#" + ((Math.random() * 0xffffff) << 0).toString(16), []);
  const randomColour1 = "#" + getRandomColor();
  const randomColour2 = "#" + getRandomColor();
  const incr = () => setCount(count + 1);

  return (
    <div style={childStyle}>
      <p>Текущее число {count}</p>
      <button style={{ background: randomColour1 }} onClick={incr}>
        Я инкримент
      </button>
      <span style={{ background: randomColour2 }}>Я просто текст</span>

      <button onClick={() => onClick(randomColour1)}>А я просто кнопка с коллбэком</button>
      <span>Я создан {new Date().toLocaleString()}</span>
    </div>
  );
};

export default UseCallbackPage;
